#![allow(incomplete_features)]
#![feature(async_fn_in_trait)]

#[cfg(feature = "delay")]
pub mod delay;
#[cfg(feature = "digital")]
pub mod digital;
#[cfg(feature = "i2c")]
pub mod i2c;
#[cfg(feature = "pwm")]
pub mod pwm;
#[cfg(feature = "serial")]
pub mod serial;
