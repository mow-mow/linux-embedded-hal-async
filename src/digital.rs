use std::{
    error,
    fmt::{self, Display, Formatter},
    ops::{Deref, DerefMut},
    path::Path,
    time::Duration,
};

use embedded_hal::digital::{
    self, ErrorKind, ErrorType, InputPin, OutputPin, PinState, StatefulOutputPin,
    ToggleableOutputPin,
};
use embedded_hal_async::digital::Wait;
use futures::{TryFutureExt, TryStreamExt};
use gpio_cdev::{
    self, AsyncLineEventHandle, Chip, EventRequestFlags, EventType, Line, LineEventHandle,
    LineHandle, LineRequestFlags,
};
use nix::{errno::Errno, time::ClockId};

#[derive(Debug)]
pub struct LinuxPinError(gpio_cdev::Error);

impl digital::Error for LinuxPinError {
    fn kind(&self) -> ErrorKind {
        ErrorKind::Other
    }
}

impl From<gpio_cdev::Error> for LinuxPinError {
    fn from(value: gpio_cdev::Error) -> Self {
        Self(value)
    }
}

impl Display for LinuxPinError {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "Linux pin error: {}", self.0)
    }
}

impl error::Error for LinuxPinError {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        Some(&self.0)
    }
}

pub struct LinuxOutputPin(LineHandle);

impl LinuxOutputPin {
    pub fn new<P>(path: P, offset: u32) -> Result<Self, LinuxPinError>
    where
        P: AsRef<Path>,
    {
        let mut chip = Chip::new(path)?;
        let line = chip.get_line(offset)?;
        line.try_into()
    }

    pub fn with_options<P>(
        path: P,
        offset: u32,
        flags: LineRequestFlags,
        default: PinState,
        consumer: &str,
    ) -> Result<Self, LinuxPinError>
    where
        P: AsRef<Path>,
    {
        let mut chip = Chip::new(path)?;
        let line = chip.get_line(offset)?;
        Self::request(line, flags, default, consumer)
    }

    fn request(
        line: Line,
        mut flags: LineRequestFlags,
        default: PinState,
        consumer: &str,
    ) -> Result<Self, LinuxPinError> {
        flags.insert(LineRequestFlags::OUTPUT);
        let default = match default {
            PinState::Low => 0,
            PinState::High => 1,
        };
        let handle = line.request(flags, default, consumer)?;
        Ok(handle.into())
    }
}

impl ErrorType for LinuxOutputPin {
    type Error = LinuxPinError;
}

impl OutputPin for LinuxOutputPin {
    fn set_low(&mut self) -> Result<(), Self::Error> {
        Ok(self.0.set_value(0)?)
    }

    fn set_high(&mut self) -> Result<(), Self::Error> {
        Ok(self.0.set_value(1)?)
    }

    fn set_state(&mut self, state: PinState) -> Result<(), Self::Error> {
        let value = match state {
            PinState::Low => 0,
            PinState::High => 1,
        };
        Ok(self.0.set_value(value)?)
    }
}

impl StatefulOutputPin for LinuxOutputPin {
    fn is_set_high(&self) -> Result<bool, Self::Error> {
        // FIXME: This actually reads the electrical state of the pin. This is against the trait's
        // actual docmentation.
        Ok(match self.0.get_value()? {
            0 => false,
            1 => true,
            _ => unreachable!(),
        })
    }

    fn is_set_low(&self) -> Result<bool, Self::Error> {
        // FIXME: This actually reads the electrical state of the pin. This is against the trait's
        // actual docmentation.
        Ok(match self.0.get_value()? {
            0 => true,
            1 => false,
            _ => unreachable!(),
        })
    }
}

impl ToggleableOutputPin for LinuxOutputPin {
    fn toggle(&mut self) -> Result<(), Self::Error> {
        match self.0.get_value()? {
            0 => self.set_high(),
            1 => self.set_low(),
            _ => unreachable!(),
        }
    }
}

impl From<LineHandle> for LinuxOutputPin {
    fn from(value: LineHandle) -> Self {
        Self(value)
    }
}

impl TryFrom<Line> for LinuxOutputPin {
    type Error = LinuxPinError;

    fn try_from(value: Line) -> Result<Self, Self::Error> {
        Self::request(value, LineRequestFlags::empty(), PinState::Low, "")
    }
}

impl Deref for LinuxOutputPin {
    type Target = LineHandle;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for LinuxOutputPin {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

pub struct LinuxInputPin(LineEventHandle);

impl LinuxInputPin {
    pub fn new<P>(path: P, offset: u32) -> Result<Self, LinuxPinError>
    where
        P: AsRef<Path>,
    {
        let mut chip = Chip::new(path)?;
        let line = chip.get_line(offset)?;
        line.try_into()
    }

    pub fn with_options<P>(
        path: P,
        offset: u32,
        flags: LineRequestFlags,
        consumer: &str,
    ) -> Result<Self, LinuxPinError>
    where
        P: AsRef<Path>,
    {
        let mut chip = Chip::new(path)?;
        let line = chip.get_line(offset)?;
        Self::events(line, flags, consumer)
    }

    fn events(
        line: Line,
        mut flags: LineRequestFlags,
        consumer: &str,
    ) -> Result<Self, LinuxPinError> {
        flags.insert(LineRequestFlags::INPUT);
        let handle = line.events(flags, EventRequestFlags::empty(), consumer)?;
        Ok(handle.into())
    }
}

impl ErrorType for LinuxInputPin {
    type Error = LinuxPinError;
}

impl InputPin for LinuxInputPin {
    fn is_high(&self) -> Result<bool, Self::Error> {
        Ok(match self.0.get_value()? {
            0 => false,
            1 => true,
            _ => unreachable!(),
        })
    }

    fn is_low(&self) -> Result<bool, Self::Error> {
        Ok(match self.0.get_value()? {
            0 => true,
            1 => false,
            _ => unreachable!(),
        })
    }
}

impl From<LineEventHandle> for LinuxInputPin {
    fn from(value: LineEventHandle) -> Self {
        Self(value)
    }
}

impl TryFrom<Line> for LinuxInputPin {
    type Error = LinuxPinError;

    fn try_from(value: Line) -> Result<Self, Self::Error> {
        Self::events(value, LineRequestFlags::empty(), "")
    }
}

impl Deref for LinuxInputPin {
    type Target = LineEventHandle;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for LinuxInputPin {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

fn now() -> nix::Result<u64> {
    let time_spec = ClockId::CLOCK_MONOTONIC.now()?;
    let now = Duration::from_secs(time_spec.tv_sec() as u64)
        + Duration::from_nanos(time_spec.tv_nsec() as u64);
    Ok(now.as_nanos() as u64)
}

#[derive(Debug)]
pub enum LinuxWaitPinError {
    Gpio(gpio_cdev::Error),
    Clock(Errno),
}

impl digital::Error for LinuxWaitPinError {
    fn kind(&self) -> ErrorKind {
        ErrorKind::Other
    }
}

impl Display for LinuxWaitPinError {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            Self::Gpio(err) => write!(f, "Linux pin error: {}", err),
            Self::Clock(err) => write!(f, "Linux clock error: {}", err),
        }
    }
}

impl error::Error for LinuxWaitPinError {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        match self {
            Self::Gpio(err) => Some(err),
            Self::Clock(err) => Some(err),
        }
    }
}

pub struct LinuxWaitPin(AsyncLineEventHandle);

impl LinuxWaitPin {
    pub fn new<P>(path: P, offset: u32) -> Result<Self, LinuxPinError>
    where
        P: AsRef<Path>,
    {
        let mut chip = Chip::new(path)?;
        let line = chip.get_line(offset)?;
        line.try_into()
    }

    pub fn with_options<P>(
        path: P,
        offset: u32,
        flags: LineRequestFlags,
        consumer: &str,
    ) -> Result<Self, LinuxPinError>
    where
        P: AsRef<Path>,
    {
        let mut chip = Chip::new(path)?;
        let line = chip.get_line(offset)?;
        Self::async_events(line, flags, consumer)
    }

    fn async_events(
        line: Line,
        mut flags: LineRequestFlags,
        consumer: &str,
    ) -> Result<Self, LinuxPinError> {
        flags.insert(LineRequestFlags::INPUT);
        let handle = line.async_events(flags, EventRequestFlags::BOTH_EDGES, consumer)?;
        Ok(handle.into())
    }
}

impl ErrorType for LinuxWaitPin {
    type Error = LinuxWaitPinError;
}

impl Wait for LinuxWaitPin {
    async fn wait_for_high(&mut self) -> Result<(), Self::Error> {
        // Recording current timestamp for later comparison.
        let now = now().map_err(LinuxWaitPinError::Clock)?;

        let handle = self.0.as_ref();
        match handle.get_value().map_err(LinuxWaitPinError::Gpio)? {
            0 => (),
            1 => return Ok(()),
            _ => unreachable!(),
        };

        while let Some(event) = self.0.try_next().map_err(LinuxWaitPinError::Gpio).await? {
            // Checking if the event is newer than the timestamp recorded earlier to ensure that
            // the event happened after the call.
            if event.timestamp() >= now {
                return Ok(());
            }
        }
        unreachable!()
    }

    async fn wait_for_low(&mut self) -> Result<(), Self::Error> {
        // Recording current timestamp for later comparison.
        let now = now().map_err(LinuxWaitPinError::Clock)?;

        let handle = self.0.as_ref();
        match handle.get_value().map_err(LinuxWaitPinError::Gpio)? {
            0 => return Ok(()),
            1 => (),
            _ => unreachable!(),
        };

        while let Some(event) = self.0.try_next().map_err(LinuxWaitPinError::Gpio).await? {
            // Checking if the event is newer than the timestamp recorded earlier to ensure that
            // the event happened after the call.
            if event.timestamp() >= now {
                return Ok(());
            }
        }
        unreachable!()
    }

    async fn wait_for_rising_edge(&mut self) -> Result<(), Self::Error> {
        // Recording current timestamp for later comparison.
        let now = now().map_err(LinuxWaitPinError::Clock)?;

        while let Some(event) = self.0.try_next().map_err(LinuxWaitPinError::Gpio).await? {
            // Checking if the event is newer than the timestamp recorded earlier to ensure that
            // the event happened after the call. And also checking if the event is the right edge.
            if event.timestamp() >= now && event.event_type() == EventType::RisingEdge {
                return Ok(());
            }
        }
        unreachable!()
    }

    async fn wait_for_falling_edge(&mut self) -> Result<(), Self::Error> {
        // Recording current timestamp for later comparison.
        let now = now().map_err(LinuxWaitPinError::Clock)?;

        while let Some(event) = self.0.try_next().map_err(LinuxWaitPinError::Gpio).await? {
            // Checking if the event is newer than the timestamp recorded earlier to ensure that
            // the event happened after the call. And also checking if the event is the right edge.
            if event.timestamp() >= now && event.event_type() == EventType::FallingEdge {
                return Ok(());
            }
        }
        unreachable!()
    }

    async fn wait_for_any_edge(&mut self) -> Result<(), Self::Error> {
        // Recording current timestamp for later comparison.
        let now = now().map_err(LinuxWaitPinError::Clock)?;

        while let Some(event) = self.0.try_next().map_err(LinuxWaitPinError::Gpio).await? {
            // Checking if the event is newer than the timestamp recorded earlier to ensure that
            // the event happened after the call.
            if event.timestamp() >= now {
                return Ok(());
            }
        }
        unreachable!()
    }
}

impl From<AsyncLineEventHandle> for LinuxWaitPin {
    fn from(value: AsyncLineEventHandle) -> Self {
        Self(value)
    }
}

impl TryFrom<LineEventHandle> for LinuxWaitPin {
    type Error = LinuxPinError;

    fn try_from(value: LineEventHandle) -> Result<Self, Self::Error> {
        let handle = AsyncLineEventHandle::new(value)?;
        Ok(handle.into())
    }
}

impl TryFrom<Line> for LinuxWaitPin {
    type Error = LinuxPinError;

    fn try_from(value: Line) -> Result<Self, Self::Error> {
        Self::async_events(value, LineRequestFlags::empty(), "")
    }
}

impl Deref for LinuxWaitPin {
    type Target = AsyncLineEventHandle;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for LinuxWaitPin {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
