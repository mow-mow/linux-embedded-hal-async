use std::{
    error,
    fmt::{self, Display, Formatter},
    future::Future,
    marker::PhantomData,
    mem::{self, ManuallyDrop},
    ops::{Deref, DerefMut},
    path::Path,
    pin::Pin,
    task::{Context, Poll},
};

use async_std::task::{self, JoinHandle};
use embedded_hal_async::i2c::{
    self, ErrorKind, ErrorType, I2c, NoAcknowledgeSource, Operation, SevenBitAddress, TenBitAddress,
};
use futures::{
    future::{Fuse, FusedFuture},
    FutureExt,
};
use i2cdev::{
    core::{I2CMessage, I2CTransfer},
    linux::{I2CMessageFlags, LinuxI2CBus, LinuxI2CError, LinuxI2CMessage},
};
use nix::errno::Errno;

#[derive(Debug)]
pub struct LinuxI2cError(LinuxI2CError);

impl i2c::Error for LinuxI2cError {
    fn kind(&self) -> ErrorKind {
        match self.0 {
            LinuxI2CError::Nix(err) => match err {
                // Best effort mapping of `nix::errno::Errno` to
                // `embedded_hal_async::i2c::ErrorKind` according to
                // https://docs.kernel.org/i2c/fault-codes.html.
                Errno::EBADMSG => ErrorKind::Bus,
                Errno::EBUSY => ErrorKind::Bus,
                Errno::EIO => ErrorKind::Bus,
                Errno::ETIMEDOUT => ErrorKind::Bus,
                Errno::EAGAIN => ErrorKind::ArbitrationLoss,
                Errno::ENXIO => ErrorKind::NoAcknowledge(NoAcknowledgeSource::Address),
                Errno::ENODEV => ErrorKind::NoAcknowledge(NoAcknowledgeSource::Data),
                Errno::ENOMEM => ErrorKind::Overrun,
                _ => ErrorKind::Other,
            },
            _ => ErrorKind::Other,
        }
    }
}

impl From<LinuxI2CError> for LinuxI2cError {
    fn from(value: LinuxI2CError) -> Self {
        Self(value)
    }
}

impl Display for LinuxI2cError {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "Linux I2C error: {}", self.0)
    }
}

impl error::Error for LinuxI2cError {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        Some(&self.0)
    }
}

struct Transaction<'a> {
    handle: ManuallyDrop<Fuse<JoinHandle<Result<(), LinuxI2cError>>>>,

    // `JoinHandle` contains these types under the hood.
    bus: PhantomData<&'a mut LinuxI2c>,
    operations: PhantomData<&'a mut [Operation<'a>]>,
}

impl<'a> Transaction<'a> {
    fn new(
        bus: &'a mut LinuxI2c,
        address: u16,
        operations: &'a mut [Operation<'_>],
        flags: I2CMessageFlags,
    ) -> Self {
        let mut read = I2CMessageFlags::READ;
        read.insert(flags);
        let mut write = I2CMessageFlags::empty();
        write.insert(flags);

        // We extend the lifetime to `'static`. This is safe since the `Transaction` will only be
        // valid as long as both the `bus` and `operations` are valid. And also dropping the
        // `Transaction` will cancel the task and block the current thread until the task is
        // finished.
        let bus: &mut LinuxI2c = unsafe { mem::transmute(bus) };
        let operations: &mut [Operation<'_>] = unsafe { mem::transmute(operations) };

        let handle = task::spawn_blocking(move || {
            let mut msgs: Box<[LinuxI2CMessage]> = operations
                .into_iter()
                .map(|item| match item {
                    Operation::Read(data) => LinuxI2CMessage::read(data)
                        .with_address(address)
                        .with_flags(read),
                    Operation::Write(data) => LinuxI2CMessage::write(data)
                        .with_address(address)
                        .with_flags(write),
                })
                .collect();
            bus.0.transfer(&mut msgs)?;
            Ok(())
        });

        Self {
            handle: ManuallyDrop::new(handle.fuse()),
            bus: PhantomData,
            operations: PhantomData,
        }
    }
}

impl Future for Transaction<'_> {
    type Output = Result<(), LinuxI2cError>;

    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        // Safe since the handle is not moved anywhere else.
        let handle = unsafe { self.map_unchecked_mut(|t| t.handle.deref_mut()) };
        handle.poll(cx)
    }
}

impl Drop for Transaction<'_> {
    // A `Transaction` struct should not be dropped before the task is finished.

    fn drop(&mut self) {
        if !self.handle.is_terminated() {
            // Safe since the handle will not be used after droping the `Transaction` struct.
            let handle = unsafe { ManuallyDrop::take(&mut self.handle) };
            // Block the current thread until the task is finished.
            let _ = task::block_on(handle);
        }
    }
}

pub struct LinuxI2c(LinuxI2CBus);

impl LinuxI2c {
    pub fn new<P>(path: P) -> Result<Self, LinuxI2cError>
    where
        P: AsRef<Path>,
    {
        let bus = LinuxI2CBus::new(path)?;
        Ok(bus.into())
    }
}

impl ErrorType for LinuxI2c {
    type Error = LinuxI2cError;
}

impl I2c<SevenBitAddress> for LinuxI2c {
    async fn transaction(
        &mut self,
        address: SevenBitAddress,
        operations: &mut [Operation<'_>],
    ) -> Result<(), Self::Error> {
        Transaction::new(self, address as u16, operations, I2CMessageFlags::empty()).await
    }
}

impl I2c<TenBitAddress> for LinuxI2c {
    async fn transaction(
        &mut self,
        address: TenBitAddress,
        operations: &mut [Operation<'_>],
    ) -> Result<(), Self::Error> {
        Transaction::new(
            self,
            address,
            operations,
            // Flags to enable 10-bit address mode. Not sure if this is sufficient to enable 10-bit
            // address mode.
            I2CMessageFlags::TEN_BIT_ADDRESS,
        )
        .await
    }
}

impl From<LinuxI2CBus> for LinuxI2c {
    fn from(value: LinuxI2CBus) -> Self {
        Self(value)
    }
}

impl Deref for LinuxI2c {
    type Target = LinuxI2CBus;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for LinuxI2c {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
