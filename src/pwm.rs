use std::{
    error,
    fmt::{self, Display},
    ops::{Deref, DerefMut},
};

use embedded_hal::pwm::{self, ErrorKind, ErrorType, SetDutyCycle};
use sysfs_pwm::{Polarity, Pwm};

#[derive(Debug)]
pub struct LinuxPwmError(sysfs_pwm::Error);

impl pwm::Error for LinuxPwmError {
    fn kind(&self) -> ErrorKind {
        ErrorKind::Other
    }
}

impl From<sysfs_pwm::Error> for LinuxPwmError {
    fn from(value: sysfs_pwm::Error) -> Self {
        Self(value)
    }
}

impl Display for LinuxPwmError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Linux PWM error: {}", self.0)
    }
}

impl error::Error for LinuxPwmError {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        Some(&self.0)
    }
}

pub struct LinuxPwm(Pwm);

impl LinuxPwm {
    pub fn new(chip: u32, number: u32, period: u32) -> Result<Self, LinuxPwmError> {
        Self::with_options(chip, number, period, 0, Polarity::Normal)
    }

    pub fn with_options(
        chip: u32,
        number: u32,
        period: u32,
        duty: u16,
        polarity: Polarity,
    ) -> Result<Self, LinuxPwmError> {
        let pwm = Pwm::new(chip, number)?;
        let mut pwm: Self = pwm.into();

        pwm.export()?;
        pwm.set_period_ns(period)?;
        pwm.set_duty_cycle(duty)?;
        pwm.set_polarity(polarity)?;
        pwm.enable(true)?;
        Ok(pwm)
    }
}

impl ErrorType for LinuxPwm {
    type Error = LinuxPwmError;
}

impl SetDutyCycle for LinuxPwm {
    fn get_max_duty_cycle(&self) -> u16 {
        // Use full 16-bit range.
        u16::MAX
    }

    fn set_duty_cycle(&mut self, duty: u16) -> Result<(), Self::Error> {
        self.set_duty_cycle_fraction(duty, self.get_max_duty_cycle())
    }

    fn set_duty_cycle_fully_off(&mut self) -> Result<(), Self::Error> {
        // Set duty cycle in nanoseconds to avoid unnecessary calculations and rounding errors.
        Ok(self.0.set_duty_cycle_ns(0)?)
    }

    fn set_duty_cycle_fully_on(&mut self) -> Result<(), Self::Error> {
        // Set duty cycle in nanoseconds to avoid unnecessary calculations and rounding errors.
        Ok(self.0.set_duty_cycle_ns(self.0.get_period_ns()?)?)
    }

    fn set_duty_cycle_fraction(&mut self, num: u16, denom: u16) -> Result<(), Self::Error> {
        // Translate fraction to `f32`.
        let cycle = num as f32 / denom as f32;
        Ok(self.0.set_duty_cycle(cycle)?)
    }
}

impl From<Pwm> for LinuxPwm {
    fn from(value: Pwm) -> Self {
        Self(value)
    }
}

impl Deref for LinuxPwm {
    type Target = Pwm;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for LinuxPwm {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

impl Drop for LinuxPwm {
    fn drop(&mut self) {
        let _ = self.enable(false);
        let _ = self.unexport();
    }
}
