use std::time::Duration;

use async_std::task;
use embedded_hal_async::delay::DelayUs;

pub struct LinuxDelay;

impl LinuxDelay {
    pub fn new() -> Self {
        // Doesn't do anything useful, but it's here to match this crate's API.
        Self
    }
}

impl DelayUs for LinuxDelay {
    async fn delay_ms(&mut self, ms: u32) {
        task::sleep(Duration::from_millis(ms as u64)).await;
    }

    async fn delay_us(&mut self, us: u32) {
        task::sleep(Duration::from_micros(us as u64)).await;
    }
}
