use std::{
    error,
    fmt::{self, Display, Formatter},
    io,
    mem::ManuallyDrop,
    ops::{Deref, DerefMut},
    os::fd::{AsRawFd, FromRawFd},
    path::Path,
};

use async_std::fs::File;
use embedded_hal_async::serial::{self, ErrorType};
use embedded_io::{asynch, blocking::ReadExactError, Io};
use futures::{AsyncReadExt, AsyncWriteExt, TryFutureExt};
use serialport::{DataBits, FlowControl, Parity, SerialPortBuilder, StopBits, TTYPort};

#[derive(Debug)]
pub struct LinuxSerialError(serialport::Error);

impl embedded_io::Error for LinuxSerialError {
    fn kind(&self) -> embedded_io::ErrorKind {
        embedded_io::ErrorKind::Other
    }
}

impl serial::Error for LinuxSerialError {
    fn kind(&self) -> serial::ErrorKind {
        match self.0.kind() {
            // Best effort mapping of `std::io::ErrorKind` to
            // `embedded_hal_async::serial::ErrorKind`.
            serialport::ErrorKind::NoDevice => serial::ErrorKind::Other,
            serialport::ErrorKind::InvalidInput => serial::ErrorKind::FrameFormat,
            serialport::ErrorKind::Unknown => serial::ErrorKind::Other,
            serialport::ErrorKind::Io(kind) => match kind {
                io::ErrorKind::OutOfMemory => serial::ErrorKind::Overrun,
                io::ErrorKind::InvalidData => serial::ErrorKind::FrameFormat,
                _ => serial::ErrorKind::Other,
            },
        }
    }
}

impl From<serialport::Error> for LinuxSerialError {
    fn from(value: serialport::Error) -> Self {
        Self(value)
    }
}

impl From<io::Error> for LinuxSerialError {
    fn from(value: io::Error) -> Self {
        value.into()
    }
}

impl Display for LinuxSerialError {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "Linux serial error: {}", self.0)
    }
}

impl error::Error for LinuxSerialError {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        Some(&self.0)
    }
}

pub struct LinuxSerial {
    port: TTYPort,
    file: ManuallyDrop<File>,
}

impl LinuxSerial {
    pub fn new<P>(path: P, baud_rate: u32) -> Result<Self, LinuxSerialError>
    where
        P: AsRef<Path>,
    {
        let builder = serialport::new(path.as_ref().to_string_lossy(), baud_rate);
        builder.try_into()
    }

    pub fn with_options<P>(
        path: P,
        baud_rate: u32,
        data_bits: DataBits,
        flow_control: FlowControl,
        parity: Parity,
        stop_bits: StopBits,
    ) -> Result<Self, LinuxSerialError>
    where
        P: AsRef<Path>,
    {
        let builder = serialport::new(path.as_ref().to_string_lossy(), baud_rate)
            .data_bits(data_bits)
            .flow_control(flow_control)
            .parity(parity)
            .stop_bits(stop_bits);
        builder.try_into()
    }
}

impl Io for LinuxSerial {
    type Error = LinuxSerialError;
}

impl asynch::Read for LinuxSerial {
    async fn read(&mut self, buf: &mut [u8]) -> Result<usize, Self::Error> {
        // We use an `async_std::fs::File` here instead of `serialport::TTYPort` because the
        // latter doesn't implement `AsyncRead` and `AsyncWrite`.
        Ok(self.file.read(buf).await?)
    }

    async fn read_exact(&mut self, mut buf: &mut [u8]) -> Result<(), ReadExactError<Self::Error>> {
        // We use an `async_std::fs::File` here instead of `serialport::TTYPort` because the
        // latter doesn't implement `AsyncRead` and `AsyncWrite`.
        Ok(self
            .file
            .read_exact(&mut buf)
            .map_err(|err| match err.kind() {
                io::ErrorKind::UnexpectedEof => ReadExactError::UnexpectedEof,
                _ => ReadExactError::Other(err.into()),
            })
            .await?)
    }
}

impl asynch::Write for LinuxSerial {
    async fn write(&mut self, buf: &[u8]) -> Result<usize, Self::Error> {
        // We use an `async_std::fs::File` here instead of `serialport::TTYPort` because the
        // latter doesn't implement `AsyncRead` and `AsyncWrite`.
        Ok(self.file.write(buf).await?)
    }

    async fn flush(&mut self) -> Result<(), Self::Error> {
        // We use an `async_std::fs::File` here instead of `serialport::TTYPort` because the
        // latter doesn't implement `AsyncRead` and `AsyncWrite`.
        Ok(self.file.flush().await?)
    }

    async fn write_all(&mut self, buf: &[u8]) -> Result<(), Self::Error> {
        // We use an `async_std::fs::File` here instead of `serialport::TTYPort` because the
        // latter doesn't implement `AsyncRead` and `AsyncWrite`.
        Ok(self.file.write_all(buf).await?)
    }
}

impl ErrorType for LinuxSerial {
    type Error = LinuxSerialError;
}

impl serial::Write for LinuxSerial {
    // Trait from `embedded_io` crate should be used instead. Keeping this for compatibility.

    async fn write(&mut self, words: &[u8]) -> Result<(), Self::Error> {
        // We use an `async_std::fs::File` here instead of `serialport::TTYPort` because the
        // latter doesn't implement `AsyncRead` and `AsyncWrite`.
        Ok(self.file.write_all(words).await?)
    }

    async fn flush(&mut self) -> Result<(), Self::Error> {
        // We use an `async_std::fs::File` here instead of `serialport::TTYPort` because the
        // latter doesn't implement `AsyncRead` and `AsyncWrite`.
        Ok(self.file.flush().await?)
    }
}

impl From<TTYPort> for LinuxSerial {
    fn from(value: TTYPort) -> Self {
        let port = value;
        let fd = port.as_raw_fd();
        let file = ManuallyDrop::new(
            // Safe since ManuallyDrop prevents the destructor from running and thus closing the
            // file descriptor twice when dropping the LinuxSerial instance.
            unsafe { File::from_raw_fd(fd) },
        );
        Self { port, file }
    }
}

impl TryFrom<SerialPortBuilder> for LinuxSerial {
    type Error = LinuxSerialError;

    fn try_from(value: SerialPortBuilder) -> Result<Self, Self::Error> {
        let port = value.open_native()?;
        Ok(port.into())
    }
}

impl Deref for LinuxSerial {
    type Target = TTYPort;

    fn deref(&self) -> &Self::Target {
        &self.port
    }
}

impl DerefMut for LinuxSerial {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.port
    }
}
